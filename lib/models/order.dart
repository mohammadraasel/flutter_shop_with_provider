import 'package:flutter_shop/models/cart.dart' show CartItem;

class Order {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  Order({this.id, this.dateTime, this.products, this.amount});
}


