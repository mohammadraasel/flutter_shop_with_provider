import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_shop/models/cart.dart' show Cart;
import 'package:flutter_shop/widgets/cart_item.dart';
import 'package:flutter_shop/providers/orders.dart';

class CartScreen extends StatelessWidget {
  static String pathName = "cart_screen";

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Cart'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(14),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Total',
                    style: TextStyle(fontSize: 20),
                  ),
                  Chip(
                    label: Text(
                      '\$${cart.totalAmount.toStringAsFixed(2)}',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  FlatButton(
                    textColor: Theme.of(context).primaryColor,
                    child: Text(
                      'Order Now',
                      style: TextStyle(
                        fontSize: 17,
                      ),
                    ),
                    onPressed: () {
                      //add cartItems to orders
                      Provider.of<Orders>(context, listen: false).addOrder(
                        cartProducts: cart.items.values.toList(),
                        total: cart.totalAmount,
                      );
                      cart.clearCart();
                    },
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: cart.itemCount,
              itemBuilder: (context, index) {
                final cartItem = cart.items.values.toList()[index];
                final productId = cart.items.keys.toList()[index];
                return CartItem(
                  title: cartItem.title,
                  id: cartItem.id,
                  price: cartItem.price,
                  quantity: cartItem.quantity,
                  productId: productId,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
