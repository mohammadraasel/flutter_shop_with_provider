import 'package:flutter/material.dart';
import 'package:flutter_shop/providers/products.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class ProductDetailScreen extends StatelessWidget {
  static String pathName = 'product-detail';

  @override
  Widget build(BuildContext context) {
    final String productId =
        ModalRoute.of(context).settings.arguments.toString();
    final product =
        Provider.of<Products>(context, listen: false).getProductById(productId);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          product?.title,
          style: GoogleFonts.solway(fontSize: 25),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              top: 10,
              right: 10,
              left: 10,
            ),
            height: 300,
            width: double.infinity,
            child: Image.network(product.imageUrl, fit: BoxFit.contain),
          ),
          SizedBox(
            height: 10,
          ),
          Text('\$${product.price}',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 20,
              )),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            child: Text(
              product.description,
              textAlign: TextAlign.center,
              softWrap: true,
              style: TextStyle(fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }
}
