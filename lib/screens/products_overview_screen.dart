import 'package:flutter/material.dart';
import 'package:flutter_shop/models/cart.dart';
import 'package:flutter_shop/screens/cart_screen.dart';
import 'package:flutter_shop/widgets/app_drawer.dart';
import 'package:flutter_shop/widgets/badge.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_shop/widgets/products_grid.dart';
import 'package:provider/provider.dart';

enum FilterOptions { Favorites, All }

class ProductsOverviewScreen extends StatefulWidget {
  static String pathName = 'product-overview';

  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  bool showFavoriteOnly = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Shop',
          style: GoogleFonts.solway(
            fontSize: 25,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          Consumer<Cart>(
            builder: (context, cart, cld) {
              return Badge(
                value: cart.itemCount.toString(),
                child: cld,
                color: Color(0xfff53b57),
              );
            },
            child: IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pushNamed(context, CartScreen.pathName);
              },
            ),
          ),
          PopupMenuButton(
            onSelected: (FilterOptions selectedOption) {
              if (selectedOption == FilterOptions.Favorites) {
                setState(() {
                  showFavoriteOnly = true;
                });
              } else {
                setState(() {
                  showFavoriteOnly = false;
                });
              }
            },
            icon: Icon(Icons.more_vert),
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text('Only Favorite'),
                  value: FilterOptions.Favorites,
                ),
                PopupMenuItem(
                  child: Text('Show All'),
                  value: FilterOptions.All,
                ),
              ];
            },
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: ProductsGrid(showFavoriteOnly: showFavoriteOnly),
    );
  }
}
