import 'package:flutter/foundation.dart';
import 'package:flutter_shop/models/order.dart';
import 'package:flutter_shop/models/cart.dart';

class Orders with ChangeNotifier {
  List<Order> _orders = [];

  List<Order> get orders {
    return [..._orders];
  }

  int get orderCount{
    return orders.length;
  }

  void addOrder({List<CartItem> cartProducts, double total}) {
    _orders.insert(
      0,
      Order(
        id: DateTime.now().toString(),
        amount: total,
        dateTime: DateTime.now(),
        products: cartProducts,
      ),
    );
    notifyListeners();
  }
}