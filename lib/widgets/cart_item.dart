import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_shop/models/cart.dart';

class CartItem extends StatelessWidget {
  final String id;
  final String title;
  final double price;
  final int quantity;
  final String productId;

  CartItem({this.price, this.id, this.title, this.quantity, this.productId});
  @override
  Widget build(BuildContext context) {
    return Dismissible(
      onDismissed: (direction){
        Provider.of<Cart>(context, listen: false).removeItem(productId);
      },
      direction: DismissDirection.endToStart,
      key: ValueKey(id),
      background: Container(
        color: Theme.of(context).errorColor,
        child: Icon(Icons.delete, color: Colors.white, size: 40,),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 10),
        margin: EdgeInsets.symmetric(horizontal: 14, vertical: 4),
      ),
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 14, vertical: 4,),
        child: Padding(
          padding: EdgeInsets.all(8),

          child: ListTile(
            leading: CircleAvatar(
              radius: 25,
              child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: FittedBox(child: Text('\$$price')),

            ),),
            title: Text(title),
            subtitle: Text('Total: \$${price * quantity}'),
            trailing: Text('$quantity x'),
          ),
        ),
      ),
    );
  }
}
