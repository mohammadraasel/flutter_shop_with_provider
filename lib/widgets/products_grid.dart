import 'package:flutter/material.dart';
import 'package:flutter_shop/widgets/product_item.dart';
import 'package:provider/provider.dart';
import 'package:flutter_shop/providers/products.dart';

class ProductsGrid extends StatelessWidget {
  final showFavoriteOnly;

  ProductsGrid({this.showFavoriteOnly});

  @override
  Widget build(BuildContext context) {
    return Consumer<Products>(
      builder: (BuildContext context, Products productData, child) {
        final products = showFavoriteOnly? productData.favoriteItems: productData.items;
        return GridView.builder(
          padding: const EdgeInsets.all(10),
          itemCount: products.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
          ),
          itemBuilder: (BuildContext context, int index) {
            return ChangeNotifierProvider.value(
              value: products[index],
              child: ProductItem(),
              );
          },
        );
      },
    );
  }
}
