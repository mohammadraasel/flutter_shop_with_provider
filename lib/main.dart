import 'package:flutter/material.dart';
import 'package:flutter_shop/models/cart.dart';
import 'package:flutter_shop/providers/orders.dart';
import 'package:flutter_shop/providers/products.dart';
import 'package:flutter_shop/screens/cart_screen.dart';
import 'package:flutter_shop/screens/orders_screen.dart';
import 'package:flutter_shop/screens/product_detail_screen.dart';
import 'package:flutter_shop/screens/products_overview_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (BuildContext context) {
          return Products();
        }),
        ChangeNotifierProvider(create: (BuildContext context) {
          return Cart();
        }),
        ChangeNotifierProvider(create: (BuildContext context) {
          return Orders();
        }),
      ],
      child: MaterialApp(
        title: 'Flutter Shop App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: MaterialColor(
              Colors.green.shade500.value,
              {
                50: Colors.green.shade50,
                100: Colors.green.shade100,
                200: Colors.green.shade200,
                300: Colors.green.shade300,
                400: Colors.green.shade400,
                500: Colors.green.shade500,
                600: Colors.green.shade600,
                700: Colors.green.shade700,
                800: Colors.green.shade800,
                900: Colors.green.shade900
              }
          ),
          accentColor: Colors.yellow,
          textTheme: Theme.of(context).textTheme.copyWith(
              body1: GoogleFonts.solway(), body2: GoogleFonts.solway()),
        ),
        initialRoute: ProductsOverviewScreen.pathName,
        routes: {
          ProductsOverviewScreen.pathName: (context) =>
              ProductsOverviewScreen(),
          ProductDetailScreen.pathName: (context) => ProductDetailScreen(),
          CartScreen.pathName: (context)=> CartScreen(),
          OrdersScreen.pathName: (context)=> OrdersScreen(),
        },
      ),
    );
  }
}
